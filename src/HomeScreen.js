// HomeScreen.js
import React from "react";
import { View, Button, StyleSheet, Text } from "react-native";

export default function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Bem-vindo!</Text>
      <View style={styles.buttonContainer}>
        <Button
          title="Count Moviment"
          onPress={() => navigation.navigate("CountMoviment")}
          color="#841584"
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          title="Image Moviment"
          onPress={() => navigation.navigate("ImageMoviment")}
          color="#841584"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f0f0f0",
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  buttonContainer: {
    width: "100%",
    marginVertical: 10,
  },
});
