import React, { useRef, useEffect, useState } from "react";
import { View, Dimensions, PanResponder, Text } from "react-native";

const CountMovement = () => {
    const [count, setCount] = useState(0);
    const [color, setColor] = useState("blue"); // Estado para controlar a cor

    const screenHeight = Dimensions.get("window").height;
    const gestureThreshold = screenHeight * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {
                // Mudar a cor para "red" se o gesto for para baixo e "blue" se for para cima
                setColor(gestureState.dy > 0 ? "red" : "blue");
            },
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dy < -gestureThreshold) {
                    setCount((prevCount) => prevCount + 1);
                }
            },
        })
    ).current;

    return (
        <View {...panResponder.panHandlers} style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: color }}>
            <Text>Valor do Contador: {count}</Text>
        </View>
    );
};

export default CountMovement;
