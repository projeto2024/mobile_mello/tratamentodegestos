import React, { useState } from "react";
import { View, Image, StyleSheet, PanResponder, Animated } from "react-native";

const ImageCarousel = () => {
  const images = [
    "https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/bltb39bbf02afd7486f/639ce77ef530196d7f93a9f4/Pel%C3%A9_1970.jpg?auto=webp&format=pjpg&width=1200&quality=60",
    "https://classic.exame.com/wp-content/uploads/2021/02/CULTURA-4-1.jpg?quality=70&strip=info&w=1024",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQitRaoyLWxNwfJzrKPw8qzBSDWZYNvOGMX3LAwoscjv9nv9OG31jx7xSt0-SsG_A0tgN8&usqp=CAU",
  ];
  const [index, setIndex] = useState(0);
  const pan = useState(new Animated.ValueXY())[0];

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: Animated.event([
      null,
      { dx: pan.x, dy: pan.y },
    ], { useNativeDriver: false }),
    onPanResponderRelease: (e, gestureState) => {
      if (gestureState.dx > 50) {
        setIndex((prevIndex) => (prevIndex - 1 + images.length) % images.length);
      } else if (gestureState.dx < -50) {
        setIndex((prevIndex) => (prevIndex + 1) % images.length);
      }
      Animated.spring(pan, { toValue: { x: 0, y: 0 }, useNativeDriver: false }).start();
    },
  });

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          styles.imageContainer,
          {
            transform: [{ translateX: pan.x }],
          },
        ]}
        {...panResponder.panHandlers}
      >
        <Image
          source={{ uri: images[index] }}
          style={styles.image}
        />
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f0f0f0",
  },
  imageContainer: {
    width: 200,
    height: 200,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
  },
});

export default ImageCarousel;
